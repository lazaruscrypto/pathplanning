QT += core
QT -= gui

CONFIG += c++11

TARGET = pathplanning
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    cell.cpp \
    tinyxml.cpp \
    tinyxmlerror.cpp \
    tinyxmlparser.cpp \
    tinystr.cpp \
    graphgrid.cpp \
    point.cpp

HEADERS += \
    graphgrid.h \
    cell.h \
    tinystr.h \
    tinyxml.h \
    point.h
