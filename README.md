# Pathplanning project (Lazarev Vladislav Alexandrovich, FCS AMI 151)

## Task

Given a map. You have queries: calculate distance between two given points and draw the best path on the map. 
Map can contain obstacles: rivers, buildings, maybe even specific landscape.

## Actualization

Fast search of shortest routes is essential in maps' functionality nowadays. Mostly they implemented by some modifications of standard shortest paths searching graph algorithms on the grids, which correspond to a source map. Such as, A*, D*, JPS, Theta*, genetic algorithms, randomized algorithms. We choose C++ as one of the fastest languages with tones of libraries (set, queue, vector), Qt as our primary desktop application (a lot of additional libraries, ease of building the whole project) and XML for maps data, as XML is the most popular way of storing data.

## Data

Maps in the xml format.

## Solution

Consider a grid n x m that will divide our map onto blocks. Some blocks are forbidden - it means that there is obstacle
in this block, so we can't go through it. We can easily count the distance between two blocks on the given metric (euclid distance between blocks)
using standard graph-search algorithms as dijkstra and it's modifications. So, for two points we find blocks, which contain
them, calculate distance between these blocks and add distance from points to blocks' boards. Increasing n and m, we can obtain
needed approximation of the path (it will strive to shortest path on the map). But, of course proper n and m can be too
big and we'll need intelligent methods.

## Realization

We need to get data from xml files. Let's work on tags, find obstacles, then let's fix n and m and create grid on the map,
that divides this map onto blocks. Then we receive a query and calculate the distance by dijkstra, also remembering the bast
ancestor for each vertex (that gives the best path to this vertex) and by this we can restore the best path and draw it
on the map using some xml features.


## Plan

Will try to finish most of the job even this month. Not seeing any difficulties with the task. Just implement different
path graph searching algorithms (3 days), work with xml (2 days), combine (2 days). 

1) Install and handle Qt, create git and write readme, enroll to the project development (8th December)

2) Implement path searching algorithms on the specific grid (n x m - size and some forbidden cells): A*, Theta*, JPS, their modifications, maybe some randomize and genetic algorithms. (20 March)

3) Work with data sets with xml (20 March)

4) Combine 2 and 3 and visualize results. (20 May - ?)

#Documentation
## Functionality :

1) Implemented input and output xml data files in main.cpp with functions getdata and outdata.


2) graphgrid.h and graphgrid.cpp contain Graph Grid implementation with includes handling the data in nxm matrix, few algorithms like JPS, A*, Theta* and the main function: calculate_distance between two vertixes (startx, starty) and (finishx, finishy) with different parameters: breakingties ("g-min" or "g-max", allowsqueeze, allowdiagonal, different points, searchtype (which is "theta" or "a-star" or "jp_search").


3) The input must be in xml format like the map [Example](https://bitbucket.org/lazar_vladis/pathplanning/issues/2/example-of-xml-input_file)


4) The output will be - [Example](https://bitbucket.org/lazar_vladis/pathplanning/issues/3/output-example)


5) To compile it you need to use Qt Creator 5.7+ and build it with arguments: path to the map xml file.


6) As the output you'll get file with the name of yours but plus _log, which means the answer. It will contain the path on the map, two list of steps: lplevel and hplevel, length of the path, time of the algorithm, mapfilename, loglevel and some additional parameters nodescreated and numberofsteps of algothim for better understanding.